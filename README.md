Role Name
=========

pgbackrestbase

Definition
------------

This role can be used in order to install and configure(base) pgbackrest tool. This tool will be used to manage postgresql backups and WAL archiving.
pgbackrestbase role supports the following OS distros;

1. Ubuntu 20.04

Requirements
------------

Make sure the following packages are installed on operating system before using the role.

* git >= 2.5.1
* curl >= 7.68.0
* wget >= 1.20.3
* ansible >= 2.9.6
* python >= 3.8.10

Role Variables
--------------

pgbackrest and  postgresql version can be defined as a variable in yaml. Default values/versions that will be used as follows;

* pgbackrest_version : 2.34
* pg_version : 12

Note : postgresql is required for configuring pgbackrest with the right user and right permissions. So, it is not necessary to change the postgresql version.

Example Playbook
----------------

``` yml
---
- hosts: localhost
  become: true
  connection: local
  roles:
    - pgbackrestbase
```
License
-------

MIT

Author Information
------------------

Hüseyin DEMİR